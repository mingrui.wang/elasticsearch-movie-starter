import os
import requests


def _simple_query(field, query):
    return { 'match': { field: query } }


def _fuzzy_query(field, query):
    return { 'fuzzy': { field: query } }


def search_movies(query, start, hits):
    uri = 'http://localhost:9200/movies/_search'
    payload = {
        'from': start, 'size': hits,
        'query': {
            'bool': {
                'should': [
                    _fuzzy_query ('title'      , query),
                    _simple_query('description', query),
                    _simple_query('actors'     , query),
                    _simple_query('genres'     , query)
                ]
            }
        }
    }

    return requests.get(uri, json=payload)
