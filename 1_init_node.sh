elasticsearch \
  -Ecluster.name=my-movie-database \
  -Enode.name=movie-node-1 \
  -Epath.data=`pwd`/data \
  -Epath.logs=`pwd`/logs \
  -Ehttp.port=9200
