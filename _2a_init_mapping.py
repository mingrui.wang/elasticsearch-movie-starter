import os
import requests


DATE_TYPE = {
    'type': 'object',
    'properties': {
        'year'     : {'type': 'integer'},
        'month'    : {'type': 'integer'},
        'day'      : {'type': 'integer'},
        'timestamp': {'type': 'integer'}
    }
}


def main():
    ELASTIC_PORT = 'http://localhost:9200'
    ALL_INDEXES = ['actors', 'genres', 'parts', 'taggings', 'movies']

    for index_name in ALL_INDEXES:
        requests.delete(os.path.join(ELASTIC_PORT, index_name))

    requests.put(os.path.join(ELASTIC_PORT, 'movies'), json={
        'mappings': {'movies': {
            'properties': {
                'id'          : {'type': 'integer'},
                'external_id' : {'type': 'integer'},
                'title'       : {'type': 'text'   },
                'description' : {'type': 'text', 'analyzer': 'english'},
                'budget'      : {'type': 'integer'},
                'picture'     : {'type': 'keyword'},
                'release_date': DATE_TYPE,
                'actors'      : {'type': 'text'   },
                'genres'      : {'type': 'text', 'analyzer': 'english'}
            }
        }},
        'settings': {
            'number_of_shards': 5,
            'number_of_replicas': 0
        }
    })


if __name__ == '__main__':
    main()
