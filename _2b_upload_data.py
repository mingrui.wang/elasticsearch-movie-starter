import os
import six
import tqdm
import datetime
import collections

import numpy as np
import pandas as pd

# We need someway to communitate with elastic, there are 2 ways, by sending
# http requests or through the elastic client interface
import requests

# There is an open source movie database stored in sqlite3 format
import sqlite3


ELASTIC_PORT = 'http://localhost:9200'
ALL_INDEXES = ['actors', 'genres', 'parts', 'taggings', 'movies']
DATABASE_PATH = './db/movies.sqlite3'


def encode(obj):
    if isinstance(obj, six.string_types):
        return obj.encode()
    elif isinstance(obj, collections.Mapping):
        return {k: encode(v) for k, v in obj.items()}
    elif isinstance(obj, collections.Sequence):
        return [encode(s) for s in obj]

    return obj


def create_connection(db_file):
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except sqlite3.Error as e:
        print(e)

    return None


def load_table(conn, table_name):
    cur = conn.cursor()

    # Get table column names
    cur.execute('PRAGMA table_info({});'.format(table_name))
    col_infos = cur.fetchall()
    col_names = [i[1] for i in col_infos]

    # Get table data
    cur.execute('SELECT * FROM {}'.format(table_name))
    table_data = cur.fetchall()

    # Format table data into df
    df = pd.DataFrame(table_data, columns=col_names)

    return df


def load_movie_id_to_actor_names():
    conn = create_connection(DATABASE_PATH)

    with conn:
        actors_df = load_table(conn, 'actors')
        parts_df  = load_table(conn, 'parts')

    pos_id   = np.argwhere(actors_df.columns == 'id')[0, 0]
    pos_name = np.argwhere(actors_df.columns == 'name')[0, 0]
    actor_id_to_name = {}
    for entry in actors_df.values:
        actor_id = entry[pos_id]
        actor_name = entry[pos_name]

        actor_id_to_name[actor_id] = actor_name

    pos_movie = np.argwhere(parts_df.columns == 'movie_id')[0, 0]
    pos_actor = np.argwhere(parts_df.columns == 'actor_id')[0, 0]
    movie_id_to_actor_names = {}
    for entry in parts_df.values:
        movie_id = entry[pos_movie]
        actor_id = entry[pos_actor]
        actor_name = actor_id_to_name[actor_id]

        if movie_id in movie_id_to_actor_names:
            movie_id_to_actor_names[movie_id] += [actor_name]
        else:
            movie_id_to_actor_names[movie_id] = [actor_name]

    return movie_id_to_actor_names


def load_movie_id_to_genres():
    conn = create_connection(DATABASE_PATH)

    with conn:
        genres_df = load_table(conn, 'genres')
        taggings_df = load_table(conn, 'taggings')

    pos_id   = np.argwhere(genres_df.columns == 'id')[0, 0]
    pos_name = np.argwhere(genres_df.columns == 'name')[0, 0]
    genre_id_to_name = {}
    for entry in genres_df.values:
        genre_id = entry[pos_id]
        genre_name = entry[pos_name]

        genre_id_to_name[genre_id] = genre_name

    pos_movie = np.argwhere(taggings_df.columns == 'movie_id')[0, 0]
    pos_genre = np.argwhere(taggings_df.columns == 'genre_id')[0, 0]
    movie_id_to_genres = {}
    for entry in taggings_df.values:
        movie_id = entry[pos_movie]
        genre_id = entry[pos_genre]
        genre_name = genre_id_to_name[genre_id]

        if movie_id in movie_id_to_genres:
            movie_id_to_genres[movie_id] += [genre_name]
        else:
            movie_id_to_genres[movie_id] = [genre_name]

    return movie_id_to_genres


def main():
    movie_id_to_actor_names = load_movie_id_to_actor_names()
    movie_id_to_genres = load_movie_id_to_genres()

    conn = create_connection(DATABASE_PATH)
    with conn:
        movies_df = load_table(conn, 'movies')

    master_df = movies_df.copy()
    master_df['release_date'] = master_df['release_date'].apply(lambda x: None if pd.isnull(x) else datetime.datetime.strptime(x, '%Y-%m-%d %H:%M:%S.%f'))
    master_df['release_date'] = master_df['release_date'].apply(lambda x: {
        'year'   : x.year,
        'month'  : x.month,
        'day'    : x.day,
        'curtime': datetime.datetime.timestamp(x)
    } if not pd.isnull(x) else None)

    master_df['actors'] = master_df['id'].apply(lambda x: movie_id_to_actor_names[x] if x in movie_id_to_actor_names else [])
    master_df['genres'] = master_df['id'].apply(lambda x: movie_id_to_genres[x] if x in movie_id_to_genres else [])

    master_df = master_df.dropna()
    master_df.index = range(len(master_df))

    master_df_col_names = ['id', 'title', 'description', 'budget', 'release_date', 'actors', 'genres']
    master_df = master_df[master_df_col_names].copy()

    print('Now uploading movies database')
    for entry_data in tqdm.tqdm(master_df.values):
        entry = {k:v for k,v in zip(master_df_col_names, entry_data)}

        uri = os.path.join(ELASTIC_PORT, 'movies', 'movies', str(entry['id']))
        res = requests.put(uri, json=entry)


if __name__ == '__main__':
    main()
